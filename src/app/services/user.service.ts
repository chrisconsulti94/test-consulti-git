import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { hostConvini, environment } from '../../environments/environment';
import { HTTP_OPTIONS, HTTP_OPTIONS_TOKEN } from './shared.service';
import { SessionEndpoint } from './enums/endpoint.enums';
import { map } from 'rxjs/operators';

@Injectable()
export class UsuariosService {
    public url: string;

    constructor(private _http: HttpClient) {
        this.url = hostConvini;
    }

    logout(): void {
        sessionStorage.clear();
    }

    login = (
        request: TRequestDatosAuth
    ): Observable<any> => {
        const $URL = `${environment.hostBackend}${SessionEndpoint.Login}`;
        return this._http.post<any>(
            $URL,
            request,
            HTTP_OPTIONS_TOKEN
        );
    };

    auth(request: any): Observable<any> {
        const $URL = `${environment.hostBackend}${SessionEndpoint.Login}`;
        return this._http.post<any>($URL, request, HTTP_OPTIONS_TOKEN);
    }

}

export type TRequestDatosAuth = {
    usuario: string;
    password: string;
};

