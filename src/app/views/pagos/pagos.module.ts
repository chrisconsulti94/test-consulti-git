import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { PagosRoutingModule } from './pagos-routing.module';
import { DataTablesModule } from 'angular-datatables';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DocumentosComponent } from './documentos/documentos.component';

import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
// Ng2-select
import { SelectModule } from 'ng-select';
// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';
// Datepicker
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';
// RECOMMENDED
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';

@NgModule({
  imports: [
    TabsModule,
    TypeaheadModule.forRoot(),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    SelectModule,
    FormsModule,
    BsDropdownModule,
    CommonModule,
    DataTablesModule,
    NgxSpinnerModule,
    PagosRoutingModule,
  ],
  declarations: [
    DocumentosComponent]
  })

export class PagosModule { }
