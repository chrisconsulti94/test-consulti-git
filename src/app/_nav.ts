import { INavData } from '@coreui/angular';
 
export const navItems: INavData[] = [

  {
    name: 'Pagos',
    url: '/pagos',
    icon: 'icon-wallet',
    children: [
      {
        name: 'Pago alícuota',
        url: '/pagos/pago-alicuota',
        icon: 'icon-wallet',
      }]
  }
];
