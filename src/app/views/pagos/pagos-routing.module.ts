import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { DocumentosComponent } from './documentos/documentos.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Pago'
    },
    children: [
      {
        path: '',
        redirectTo: 'documento'
      },
      {
        path: 'documento',
        component: DocumentosComponent,
        data: {
          title: 'Documentos'
        }
      }
    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagosRoutingModule { }
